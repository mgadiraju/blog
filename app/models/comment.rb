class Comment < ActiveRecord::Base
  belongs_to :post
  validates_presence_of :post_id, :message => "can't be blank"
  validates_presence_of :body, :message => "can't be blank"
end
