class Post < ActiveRecord::Base
  has_many :comments, :dependent => :destroy
  validates_presence_of :title, :message => "can't be blank"
  validates_presence_of :body, :message => "can't be blank"
end
